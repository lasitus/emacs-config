;; (defun debug-on-load-obsolete (filename)
;;   (when (equal (car (last (split-string filename "[/\\]") 2))
;;                "obsolete")
;;     (debug)))
;; (add-to-list 'after-load-functions #'debug-on-load-obsolete)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
	("9b35c097a5025d5da1c97dba45fed027e4fb92faecbd2f89c2a79d2d80975181" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(org-agenda-files (quote ("~/notes/inbox.org")))
 '(org-export-backends (quote (ascii html icalendar latex md odt)))
 '(package-selected-packages
   (quote
	(yaml-mode org-super-agenda org-roam-server orgalist org-roam deft dmenu dired-du exwm zones helm-ispell magit-annex vlf org-time-budgets ag dumb-jump projectile git-gutter magit multi-term gnuplot org-journal))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(org-babel-load-file (expand-file-name "~/.emacs.d/base.org"))
(org-babel-load-file (expand-file-name "~/.emacs.d/pc.org"))
(org-babel-load-file (expand-file-name "~/.emacs.d/common.org"))

(put 'erase-buffer 'disabled nil)
