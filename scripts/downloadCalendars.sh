#!/bin/bash

# customize these
WGET=/usr/bin/wget
ICS2ORG=/home/adam/.emacs.d/ical2org
ICSFILE=/home/adam/.emacs.d/calendar/acwatkins.ics
ORGFILE=/home/adam/org/calendar.org
URL=https://calendar.google.com/calendar/ical/acwatkins%40gmail.com/private-861252e8c8fcb2037de240d8d87cf536/basic.ics

# no customization needed below

$WGET -O $ICSFILE $URL
$ICS2ORG < $ICSFILE > $ORGFILE

# # customize these
# WGET=/usr/bin/wget
# ICS2ORG=../ical2org
# ICSFILE=../calendar/abv.ics
# ORGFILE=/home/adam/org/abvCalendar.org
# URL=https://calendar.google.com/calendar/ical/adamwatkins%40allembusinessventures.com/public/basic.ics

# # no customization needed below

# $WGET -O $ICSFILE $URL
# $ICS2ORG < $ICSFILE > $ORGFILE
